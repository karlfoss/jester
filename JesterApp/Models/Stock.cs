using System;

namespace JesterApp.Models
{
    public class Stock
    {
        private const decimal BILLION = 1000000000;
        private const string NOT_AVALIABLE = "N/A";

        public CompanyOverview CompanyOverview { get; set;  }
        public Earnings Earnings { get; set; }

        public static string ToCurrency(string money, string currency)
        {
            decimal covnertedMoney;
            bool isDecimal = decimal.TryParse(money, out covnertedMoney);

            if (!isDecimal)
            {
                return NOT_AVALIABLE;
            }

            return $"({currency}) {Math.Round(covnertedMoney,3,MidpointRounding.AwayFromZero)}";
        }

        public static string ToCurrencyByBillion(string money, string currency)
        {
            decimal covnertedMoney;
            bool isDecimal = decimal.TryParse(money, out covnertedMoney);

            if (!isDecimal)
            {
                return NOT_AVALIABLE;
            }

            return $"({currency}) {Math.Round(covnertedMoney/BILLION,3,MidpointRounding.AwayFromZero)}";
        }

        public bool IsStockValid()
        {
            if (CompanyOverview == null)
            {
                return false;
            }

            return !string.IsNullOrWhiteSpace(CompanyOverview.Sector) &&
                    !string.IsNullOrWhiteSpace(CompanyOverview.MarketCapitalization) &&
                    !string.IsNullOrWhiteSpace(CompanyOverview.TickerSymbol) &&
                    !string.IsNullOrWhiteSpace(CompanyOverview.CompanyName);
        }
    }
}
