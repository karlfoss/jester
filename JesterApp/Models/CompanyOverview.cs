namespace JesterApp.Models
{
    public class CompanyOverview
    {
        public string TickerSymbol { get; set; }
        public string CompanyName { get; set; }
        public string Currency { get; set; }
        public string Sector { get; set; }
        public string Industry { get; set; }
        public string MarketCapitalization { get; set; }
        public string EBITDA { get; set; }
        public string PERatio { get; set; }
        public string PEGRatio { get; set; }
        public string BookValue { get; set; }
        public string DividendPerShare { get; set; }
        public string DividendYield { get; set; }
        public string EPS { get; set; }
        public string RevenuePerShareTTM { get; set; }
        public string ProfitMargin { get; set; }
        public string OperatingMarginTTM { get; set; }
        public string ReturnOnAssetsTTM { get; set; }
        public string ReturnOnEquityTTM { get; set; }
        public string RevenueTTM { get; set; }
        public string GrossProfitTTM { get; set; }
        public string DilutedEPSTTM { get; set; }
        public string QuarterlyEarningsGrowthYOY { get; set; }
        public string QuarterlyRevenueGrowthYOY { get; set; }
        public string AnalystTargetPrice { get; set; }
        public string TrailingPE { get; set; }
        public string ForwardPE { get; set; }
        public string PriceToSalesRatioTTM { get; set; }
        public string PriceToBookRatio { get; set; }
        public string EVToRevenue { get; set; }
        public string EVToEBITDA { get; set; }
        public string Beta { get; set; }
        public string PreviousYearHigh { get; set; }
        public string PreviousYearLow { get; set; }
        public string FiftyDayMovingAverage { get; set; }
        public string TwoHundredDayMovingAverage { get; set; }
    }
}
