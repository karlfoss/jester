﻿using JesterApp.Models;
using System.Threading.Tasks;

namespace JesterApp.Interfaces
{
    public interface IStockService
    {
        Task<CompanyOverview> GetCompanyOverviewAsync(string stockTicker);
    }
}
