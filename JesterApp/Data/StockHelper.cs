using System.Collections.Generic;
using System.Threading.Tasks;
using JesterApp.Interfaces;
using JesterApp.Models;

namespace JesterApp.Data
{
    //TODO: consider turning this into a page helper so that it is 1-1 with a given view (IE CompareStockerHelper.cs
    public class StockHelper
    {
        private IStockService _stockService;

        //TODO: consider breaking out field information into a seperate class to manage
        public Dictionary<string, bool> CompareStocksDetails { get; set; }

        public StockHelper(IStockService stockService)
        {
            _stockService = stockService;

            CompareStocksDetails = new Dictionary<string, bool>();
            CompareStocksDetails.Add("CompanyName", false);
            CompareStocksDetails.Add("BusinessSector", false);
            CompareStocksDetails.Add("MarketCapitalization", false);
        }

        public async Task<IList<Stock>> GetStockDataAsync(string[] stockTickers)
        {
            if (stockTickers.Length == 0)
            {
                return new List<Stock>();
            }

            IList<Stock> stocks = new List<Stock>();
            IList<Task<CompanyOverview>> companyOverviewTasks = new List<Task<CompanyOverview>>();
            foreach (string stockTicker in stockTickers)
            {
                companyOverviewTasks.Add(_stockService.GetCompanyOverviewAsync(stockTicker));
            }

            await Task.WhenAll(companyOverviewTasks);

            for (int i = 0; i < stockTickers.Length; i++)
            {
                Stock stock = new Stock
                {
                    CompanyOverview = companyOverviewTasks[i].Result
                };
                stocks.Add(stock);
            }

            return stocks;
        }

        public async Task<Stock> GetStockDataAsync(string stockTicker)
        {
            Task<CompanyOverview> companyOverviewTask = _stockService.GetCompanyOverviewAsync(stockTicker);

            await Task.WhenAll(companyOverviewTask);

            return new Stock
            {
                CompanyOverview = companyOverviewTask.Result
            };
        }
    }
}
