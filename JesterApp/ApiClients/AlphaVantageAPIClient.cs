﻿using JesterApp.Interfaces;
using JesterApp.Models;
using JesterApp.Models.AlphaVantage;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace JesterApp.ApiClients
{
    public class AlphaVantageAPIClient : IStockService
    {
        private const string API_KEY = "3RUCYXSOFZ3DW8I5";
        private HttpClient _httpClient;

        public AlphaVantageAPIClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<CompanyOverview> GetCompanyOverviewAsync(string stockTicker)
        {
            if (string.IsNullOrWhiteSpace(stockTicker))
            {
                return new CompanyOverview();
            }

            var response = await _httpClient.GetAsync($"https://www.alphavantage.co/query?function=OVERVIEW&symbol={stockTicker}&apikey={API_KEY}");
            response.EnsureSuccessStatusCode();

            using (HttpContent httpContent = response.Content)
            {
                string responseBody = await httpContent.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<AlphaVantageCompanyOverview>(responseBody).TransformToCompanyOverview();
            }
        }
    }
}
