**Jester**

This app has been started as a way to learn Blazor (starting with the Blazor Server template). The focus of this app all things stocks and ETFs. I picked the name Jester because you're probably a fool to try to hand pick stocks and consistently beat the market.
---

## API Sources
TODO: consider using API from my brokers
https://www.alphavantage.co/documentation/ - Implemented with free tier, but very limited and premium tier is expensive
https://developer.tradier.com/ - Implemented with free tier
https://iexcloud.io/docs/api/ - Robust, not sure if free
https://twelvedata.com/ - Not implemented yet
https://www.xignite.com/news/best-6-free-and-paid-stock-market-apis/ - more to choose from

Component Library:
https://github.com/stsrki/Blazorise

TODO: make sure attributions to the above sources are up to their code